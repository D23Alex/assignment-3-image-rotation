#pragma once

#include <bits/stdint-uintn.h>

#include "dimensions.h"
#include <inttypes.h>
#include <malloc.h>
#include <stddef.h>


struct image {
  struct dimensions size;
  struct pixel* data;
};

struct image image_create( struct dimensions size );
void image_destroy( struct image* image );
