#include "rotation.h"
#include "../include/image.h"
#include <stddef.h>


struct image initialize_target_image(struct image const source) {
    uint64_t target_height = source.width;
    uint64_t target_width = source.height;
    struct image const target = initialize_image(target_width, target_height);
    return target;
}

void put_pixel_to_correct_position_after_rotation(const struct image source, const struct image target,
        size_t source_pixel_row, size_t source_pixel_column) {
    struct pixel source_pixel = get_pixel_by_position(source, source_pixel_row, source_pixel_column);
    size_t target_pixel_column = source.height - 1 - source_pixel_row;
    size_t target_pixel_row = source_pixel_column;
    set_pixel_by_position(source_pixel, target, target_pixel_row, target_pixel_column);
}

struct image rotate(struct image const source) {
    struct image const target = initialize_target_image(source);
    for (size_t row = 0; row < source.height; row++)
        for (size_t column = 0; column < source.width; column++)
            put_pixel_to_correct_position_after_rotation(source, target, row, column);
    return target;
}
