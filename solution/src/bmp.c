#include "bmp.h"
#include "image.h"
#include <bits/types/FILE.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>


struct __attribute__((packed)) bmp_header  {
    uint16_t bfType;
    uint32_t bfileSize;
    uint32_t bfReserved;
    uint32_t bOffBits;
    uint32_t biSize;
    uint32_t biWidth;
    uint32_t biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    uint32_t biXPelsPerMeter;
    uint32_t biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
};


bmp_header defaults = {
        .bfType = 0x4d42, // bmp header starts with "BM" in ASCII
        .bfReserved = 0,
        .bOffBits = sizeof(bmp_header),
        .biSize = 40,
        .biPlanes = 1,
        .biBitCount = 24,
        .biCompression = 0,
        .biSizeImage = 0,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
};


enum read_status read_bmp_header(bmp_header *bmp_header, FILE *file) {
    size_t objects_read = fread(bmp_header, sizeof(struct bmp_header), 1, file);
    if (objects_read != 1)
        return READ_INVALID_BITS;
    return READ_OK;
}

enum read_status validate_header(bmp_header *bmp_header) {
    if (bmp_header->bfType != defaults.bfType)
        return READ_INVALID_SIGNATURE;
    bool bmp_header_valid = (bmp_header->biPlanes == defaults.biPlanes
            && bmp_header->biBitCount == defaults.biBitCount
            && bmp_header->biCompression == defaults.biCompression);
    if (bmp_header_valid)
        return READ_OK;
    return READ_INVALID_HEADER;
}

size_t calculate_padding(const uint32_t width_in_px) {
    uint32_t bytes_per_row = width_in_px * sizeof(struct pixel);
    return (4 - (bytes_per_row % 4)) % 4;
}

size_t calculate_bytes_in_row_expected(uint32_t rows, uint32_t columns, uint32_t row_index, size_t padding) {
    bool is_last_row = row_index == (rows - 1);
    if (is_last_row)
        return columns * 3;
    return columns * 3 + padding;
}

enum read_status read_bmp_image_data(struct image *img, FILE *file) {
    struct pixel *data = img->data;
    size_t padding = calculate_padding(img->width);
    for (uint32_t row_index = 0; row_index < img->height; row_index++, data+= img->width) {
        size_t bytes_expected = calculate_bytes_in_row_expected(img->height, img->width, row_index, padding);
        size_t bytes_read = fread(data, 1, bytes_expected, file);
        if (bytes_read != bytes_expected)
            return READ_INVALID_BITS;
    }
    return READ_OK;
}

enum read_status reset_stream_indicator_to_read_data(FILE *file, uint32_t offset) {
    int res = fseek(file, offset, SEEK_SET);
    if (res == 0)
        return READ_OK;
    return READ_INVALID_BITS;
}

enum read_status from_bmp(FILE *in, struct image *img) {
    bmp_header header;
    enum read_status read_header_status = read_bmp_header(&header, in);
    if (read_header_status != READ_OK)
        return read_header_status;

    enum read_status bmp_validation_status = validate_header(&header);
    if (bmp_validation_status != READ_OK)
        return bmp_validation_status;

    enum read_status reset_stream_indicator_to_read_data_status =
            reset_stream_indicator_to_read_data(in, header.bOffBits);
    if (reset_stream_indicator_to_read_data_status != READ_OK)
        return reset_stream_indicator_to_read_data_status;

    *img = initialize_image(header.biWidth, header.biHeight);
    enum read_status image_data_read_status = read_bmp_image_data(img, in);
    if (image_data_read_status != READ_OK)
        return  image_data_read_status;
    return READ_OK;
}

bmp_header initialize_header_for_image(const struct image *image, size_t padding) {
    return (bmp_header) {
            .bfType = defaults.bfType,
            .bfileSize = sizeof(bmp_header) + (image->width * 3 + padding) * image->height,
            .bfReserved = defaults.bfReserved,
            .bOffBits = defaults.bOffBits,
            .biSize = defaults.biSize,
            .biWidth = image->width,
            .biHeight = image->height,
            .biPlanes = defaults.biPlanes,
            .biBitCount = defaults.biBitCount,
            .biCompression = defaults.biCompression,
            .biSizeImage = defaults.biSizeImage,
            .biXPelsPerMeter = defaults.biXPelsPerMeter,
            .biYPelsPerMeter = defaults.biYPelsPerMeter,
            .biClrUsed = defaults.biClrUsed,
            .biClrImportant = defaults.biClrImportant
    };
}

enum write_status write_bmp_header(bmp_header *header, FILE *file) {
    size_t objects_written_expected = 1;
    size_t objects_written = fwrite(header, sizeof(bmp_header), 1, file);
    if (objects_written != objects_written_expected)
        return BMP_WRITE_ERROR_IO;
    return BMP_WRITE_OK;
}

enum write_status write_bmp_image_data(struct image const *img, FILE *file) {
    struct pixel *data = img->data;
    size_t padding = calculate_padding(img->width);
    for (uint32_t current_row_index = 0; current_row_index < img->height; current_row_index++, data+= img->width) {
        size_t bytes_expected = calculate_bytes_in_row_expected(img->height, img->width,
                                                                current_row_index, padding);
        size_t bytes_written = fwrite(data, 1, bytes_expected, file);
        if (bytes_written != bytes_expected)
            return BMP_WRITE_ERROR_IO;
    }
    return BMP_WRITE_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img ) {
    size_t padding = calculate_padding(img->width);
    bmp_header header = initialize_header_for_image(img, padding);

    enum write_status write_header_status = write_bmp_header(&header, out);
    if (write_header_status != BMP_WRITE_OK)
        return write_header_status;

    enum write_status write_image_data_status = write_bmp_image_data(img, out);
    if (write_image_data_status != BMP_WRITE_OK)
        return write_header_status;
    return BMP_WRITE_OK;
}

enum read_status read_bmp_from_file(char *file_path, struct image *image) {
    FILE *file = fopen(file_path, "rb");
    if (file == NULL)
        return READ_INVALID_BITS;
    enum read_status status = from_bmp(file, image);
    fclose(file);
    return status;

}

enum write_status write_bmp_to_file(char *file_path, struct image const* image) {
    FILE *file = fopen(file_path, "wb");
    if (file == NULL)
        return BMP_WRITE_ERROR_IO;
    enum write_status status = to_bmp(file, image);
    fclose(file);
    return status;
}
