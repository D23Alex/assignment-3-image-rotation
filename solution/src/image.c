#include "../include/image.h"
#include <malloc.h>
#include <stdlib.h>


struct pixel get_pixel_by_position(const struct image image, size_t row, size_t column) {
    return image.data[row * image.width + column];
}

void set_pixel_by_position(struct pixel pixel, const struct image image, size_t row, size_t column) {
    image.data[row * image.width + column] = pixel;
}

struct image initialize_image(const uint64_t width, const uint64_t height) {
    struct image new_image = (struct image) {.width = width, .height = height, .data = NULL};
    new_image.data = malloc(sizeof(struct pixel) * new_image.height * new_image.width);
    if (new_image.data == NULL) {
        fprintf(stderr, "Out of memory - generating core dump");
        abort();
    }
    return new_image;
}

// Passing ptr of image because setting image's data to NULL is required
void free_image(struct image *image) {
    free(image->data);
    image->data = NULL;
}
