#include "rotation.h"
#include "bmp.h"
#include "image.h"
#include <stdbool.h>
#include <stdio.h>


void report_read_status(enum read_status status) {
    switch (status) {
        case READ_OK:
            fprintf(stdout, "Success");
        case READ_INVALID_SIGNATURE:
            fprintf(stderr, "Error - Header has invalid signature");
        case READ_INVALID_HEADER:
            fprintf(stderr, "Error- Invalid header(remember, only 24 bit per pixel images supported)");
        case READ_INVALID_BITS:
            fprintf(stderr, "Error - some IO error or something");
        default:
            fprintf(stderr, "Error");
    }
}

void report_write_status(enum write_status status) {
    switch (status) {
        case BMP_WRITE_ERROR_IO:
            fprintf(stderr, "Error - could not write to file");
        case BMP_WRITE_OK:
            fprintf(stdout, "Success");
        default:
            fprintf(stdout, "Something went wrong when writing to file");
    }
}

int main(int number_of_args, char **args) {
    struct image source;
    bool enough_arguments = (number_of_args < 3);
    if (!enough_arguments) {
        fprintf(stderr, "Not enough arguments - use './image-transformer <source> <target>'");
    }

    enum read_status read_status = read_bmp_from_file(args[1], &source);
    if (read_status != READ_OK) {
        report_read_status(read_status);
        free_image(&source);
        return 1;
    }

    struct image result = rotate(source);

    enum write_status write_status = write_bmp_to_file(args[2], &result);
    if (write_status != BMP_WRITE_OK) {
        report_write_status(write_status);
        free_image(&source);
        free_image(&result);
        return 1;
    }

    free_image(&source);
    free_image(&result);
    return 0;
}
