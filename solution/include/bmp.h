#ifndef BMP_H
#define BMP_H

#include "image.h"
#include <bits/stdint-uintn.h>


struct __attribute__((packed)) bmp_header;
typedef struct bmp_header bmp_header;

enum read_status {
    READ_OK = 0,
    READ_INVALID_SIGNATURE,
    READ_INVALID_BITS,
    READ_INVALID_HEADER,
};

enum write_status  {
    BMP_WRITE_OK = 0,
    BMP_WRITE_ERROR_IO,
};


enum read_status read_bmp_from_file(char *file_path, struct image *image);

enum write_status write_bmp_to_file(char *file_path, struct image const* image);

extern uint16_t BMP_HEADER_EXPECTED_FIRST_TWO_BYTES; // bmp header starts with "BM" in ASCII


#endif //BMP_H
