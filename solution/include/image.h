#ifndef IMAGE_H
#define IMAGE_H

#include <bits/stdint-uintn.h>
#include <stddef.h>


struct __attribute__((packed)) pixel { uint8_t r, g, b; };

struct image {
    uint64_t width, height;
    struct pixel* data;
};


struct image initialize_image(uint64_t width, uint64_t height);

void free_image(struct image *image);

struct pixel get_pixel_by_position(const struct image image, size_t row, size_t column);

void set_pixel_by_position(struct pixel pixel, const struct image image, size_t row, size_t column);


#endif //IMAGE_H
